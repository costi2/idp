package com.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestClient {

    private String server;
    private String backupServer;
    private RestTemplate rest;
    private HttpHeaders headers;
    private HttpStatus status;

    public RestClient(String server, String backupServer) {
        this.server = server;
        this.backupServer = backupServer;
        this.rest = new RestTemplate();
        this.headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
    }

    public void setServer(String server) {
        this.server = server;
    }

    private String executeRestOperation(String uri, String json, HttpMethod method) {
        try {
            HttpEntity<String> requestEntity = new HttpEntity<String>(json, headers);
            ResponseEntity<String> responseEntity = rest.exchange(server + uri, method, requestEntity, String.class);
            this.setStatus(responseEntity.getStatusCode());
            return responseEntity.getBody();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                HttpEntity<String> requestEntity = new HttpEntity<String>(json, headers);
                ResponseEntity<String> responseEntity = rest.exchange(backupServer + uri, method, requestEntity, String.class);
                this.setStatus(responseEntity.getStatusCode());
                return responseEntity.getBody();
            } catch (Exception r) {
                r.printStackTrace();
                return null;
            }
        }
    }

    public String get(String uri, String json) {
        return executeRestOperation(uri, "", HttpMethod.GET);
    }

    public String post(String uri, String json) {
        return executeRestOperation(uri, json, HttpMethod.POST);
    }

    public String put(String uri, String json) {
        return executeRestOperation(uri, json, HttpMethod.PUT);
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
