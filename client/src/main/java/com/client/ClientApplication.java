package com.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class Operations {
	static private ObjectMapper objectMapper = new ObjectMapper();
	String server = null;
	String backupServer = null;
	static RestClient restClient = null;

	public Operations(String server, String backupServer) {
		this.server = server;
		this.backupServer = backupServer;
		restClient = new RestClient(server, backupServer);
	}

	static private boolean checkInput(String type, String []input) {
		return true;
	}

	static void getOptimalRoute(String input) {
		if(input.split(" ").length != 5){
			System.out.println("The number of argumets is wrong. They should be 5");
			return;
		}

		HashMap<String, String> map = new HashMap<>();
		String[] tokens = input.split(" ");
		String source = tokens[1];
		String destination = tokens[2];
		String maxFlights = tokens[3];
		String departureDay = tokens[4];
		if(!checkInput(tokens[0], tokens))
			return;

		map.put("source", source);
		map.put("destination", destination);
		map.put("maxFlights", maxFlights);
		map.put("departureDay", departureDay);

		try {
			String response = restClient.post("getOptimalRoute", convertToJson(map));
			System.out.println(response.replace("},{", "\n")
					.replace("}", "")
					.replace("{", "")
					.replace("]", "")
					.replace("[", ""));
		} catch (Exception e) {
			System.out.println("Operation failed. Try again.");
			e.printStackTrace();
		}
	}

	static private void convertToMap(String json) {
		if(json == null) {
			System.out.println("No Body Received");
			return;
		}

		HashMap<String, String> result = null;
		try {
			result = new ObjectMapper().readValue(json, HashMap.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(result != null)
			System.out.println(result);
	}

	static private String convertToJson(Map<String, String> map) {

		String json = null;
		try {
			json = objectMapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			System.err.println("[CLIENT ERROR] The creation of the json failed and the request was not sent. " +
					"Retype command");
			e.printStackTrace();
		}

		return json;
	}

	static void bookTicket(String input) {
		if(input.split(" ").length != 2){
			System.out.println("The number of argumets is wrong. They should be 2. The ids are only" +
					"separeted through space");
			return;
		}

		HashMap<String, String> map = new HashMap<>();
		String flightIds = input.split(" ")[1];
		map.put("flightIds", flightIds);

		try {
			String response = restClient.post("bookTicket", convertToJson(map));
			convertToMap(response);
		} catch (Exception e) {
			System.out.println("Operation failed. Try again.");
			e.printStackTrace();
		}
	}

	static void buyTicket(String input) {
		if(input.split(" ").length != 3){
			System.out.println("The number of argumets is wrong. They should be 3");
			return;
		}
		HashMap<String, String> map = new HashMap<>();
		String reservationId = input.split(" ")[1];
		String creditInformation = input.split(" ")[2];

		map.put("reservationId", reservationId);
		map.put("creditInformation", creditInformation);
		try {
			String response = restClient.post("buyTicket", convertToJson(map));
			System.out.println(response.replace("},{", "\n")
					.replace("}", "")
					.replace("{", "")
					.replace("]", "")
					.replace("[", ""));
		} catch (Exception e) {
			System.out.println("Operation failed. Try again.");
			e.printStackTrace();
		}
	}

	static void addFlight(String input) {
		if(input.split(" ").length != 8){
			System.out.println("The number of argumets is wrong. They should be 8");
			return;
		}
		HashMap<String, String> map = new HashMap<>();
		String[] tokens = input.split(" ");
		String source = tokens[1];
		String destination = tokens[2];
		String departure_hour = tokens[3];
		String departure_day = tokens[4];
		String flight_duration = tokens[5];
		String available_seats = tokens[6];

		if(!checkInput(tokens[0], tokens))
			return;

		System.out.println("Id is: " + tokens[7]);
		map.put("id", tokens[7]);
		map.put("source", source);
		map.put("destination", destination);
		map.put("departure_hour", departure_hour);
		map.put("departureDay", departure_day);
		map.put("flight_duration", flight_duration);
		map.put("available_seats", available_seats);

		try {
			String response = restClient.post("addFlight", convertToJson(map));
			convertToMap(response);
		} catch (Exception e) {
			System.out.println("Exception occured");
			e.printStackTrace();
		}
	}

	static void cancelFlight(String input) {
		if(input.split(" ").length != 2){
			System.out.println("The number of argumets is wrong. They should be 2.");
			return;
		}
		HashMap<String, String> map = new HashMap<>();
		String flightId = input.split(" ")[1];

		map.put("flightId", flightId);
		try {
			String response = restClient.post("cancelFlight", convertToJson(map));
			convertToMap(response);
		} catch (Exception e) {
			System.out.println("Exception occured");
			e.printStackTrace();
		}
	}

	static void showBoughtTickets(String line) {
		try {
			String response = restClient.get("BoughtTickets", "");
			System.out.println(response);
		} catch (Exception e) {
			System.out.println("Operation failed. Try again.");
			e.printStackTrace();
		}
	}

	static void showBookedTickets(String line) {
		try {
			String response = restClient.get("BookedTickets", "");
			System.out.println(response);
		} catch (Exception e) {
			System.out.println("Operation failed. Try again.");
			e.printStackTrace();
		}
	}

	static void showFlights(String line) {
		try {
			String response = restClient.get("Flights", "");
			System.out.println(response.replace("},{", "\n")
					.replace("}", "")
					.replace("{", "")
					.replace("]", "")
					.replace("[", ""));
		} catch (Exception e) {
			System.out.println("Operation failed. Try again.");
			e.printStackTrace();
		}
	}

	static String checkLogin(String username, String password) {
		HashMap<String, String> map = new HashMap<>();
		String response = null;

		map.put("username", username);
		map.put("password", password);
		map.put("role", "admin");
		try {
			response = restClient.post("checkLogin", convertToJson(map));
			//convertToMap(response);
		} catch (Exception e) {
			System.out.println("Exception occured");
			e.printStackTrace();
		}

		return response;
	}
}

interface Login {
	void loginLogic();
}

@SpringBootApplication
public class ClientApplication {

	static Login logged = null;
	static final String commands = "Possible commands for your role are: " + '\n' +
							"AddRide source destination hour day duration avb_seats id" + '\n' +
							"CancelRide RideId" + '\n' +
							"ShowRides" + '\n' +
							"logout" + '\n' +
							"info" + '\n' +
							"EXIT" + '\n';

	private static class Admin implements Login {
		static private String server = "http://serverAdmin:8080";
		static private String backupServer = "http://localhost:8080";
		private Operations operations = new Operations(server, backupServer);
		public void loginLogic() {
			Scanner input = new Scanner(System.in);
			String line = null;
			System.out.println(commands);

			while (true) {
				if (!input.hasNextLine())
					break;
				line = input.nextLine();
				if (line.startsWith("EXIT"))
					break;

				if (line.length() == 0)
					continue;

				if (line.toLowerCase().startsWith("addride")) {
					operations.addFlight(line);
				} else if (line.toLowerCase().startsWith("cancelride")) {
					operations.cancelFlight(line);
				} else if (line.toLowerCase().startsWith("showrides")) {
					operations.showFlights(line);
				} else if (line.toLowerCase().startsWith("showbookedtickets")) {
					operations.showBookedTickets(line);
				} else if (line.toLowerCase().startsWith("showboughttickets")) {
					operations.showBoughtTickets(line);
				} else if (line.toLowerCase().startsWith("info")) {
					System.out.println(commands);
				} else if (line.toLowerCase().startsWith("logout")){
					logged = new LoginOperation();
					break;
				} else {
					System.out.println("UNKOWN COMMAND!! Retype another!");
				}
			}
		}
	}

	private static class Client implements Login {
		static private String server = "http://serverClient:8081";
		static private String backupServer = "http://localhost:8081";
		private Operations operations = new Operations(server, backupServer);
		static final String commands = "Possible commands for your role are: " + '\n' +
				"OptimalRoute source destination maxStops departureDay" + '\n' +
				"BookTicket ticketId1,ticketId2,ticketId3" + '\n' +
				"BuyTicket reservationId creditInformation" + '\n' +
				"logout" + '\n' +
				"EXIT";
		public void loginLogic() {
			Scanner input = new Scanner(System.in);
			String line = null;

			System.out.println(commands);
			while (true) {
				if (!input.hasNextLine())
					break;
				line = input.nextLine();
				if (line.startsWith("EXIT"))
					break;

				if (line.length() == 0)
					continue;

				if (line.toLowerCase().startsWith("optimalroute")) {
					operations.getOptimalRoute(line);
				} else if (line.toLowerCase().startsWith("bookticket")) {
					operations.bookTicket(line);
				} else if (line.toLowerCase().startsWith("buyticket")) {
					operations.buyTicket(line);
				} else if (line.equals("logout")) {
					logged = new LoginOperation();
					break;
				} else if (line.equals("info")) {
					System.out.println(commands);
				} else {
					System.out.println("UNKOWN COMMAND!! Retype another!");
				}
			}
		}
	}

	private static class LoginOperation implements Login {
		static private String server = "http://serverClient:8081";
		static private String backupServer = "http://localhost:8080";
		private Operations operations = new Operations(server, backupServer);
		@Override
		public void loginLogic() {
			String username = null;
			String password = null;
			Scanner input = new Scanner(System.in);
			System.out.println("Login:");
			System.out.println("============================================================");
			boolean loginStatus = false;
			String userType = null;
			while(!loginStatus) {
				System.out.println("Enter username: ");
				username = input.nextLine();

				System.out.println("Enter password: ");
				password = input.nextLine();
				userType = operations.checkLogin(username, password);
				if(userType == null)
					continue;
				loginStatus = !userType.equals("failed");

				System.out.println("Login " + (loginStatus ? "succes" : "failed"));
			}
			System.out.println("============================================================");
			System.out.println("You are logged as " + userType);
			if(userType.equals("admin"))
				logged = new Admin();
			else
				logged = new Client();
		}
	}

	public static void main(String[] args) {
		logged = new LoginOperation();
		while(logged != null) {
			logged.loginLogic();
		}
	}
}
