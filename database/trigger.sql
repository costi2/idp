CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL,
  `operation_type` varchar(255) NOT NULL,
  `user` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS loginMonitoring;
DELIMITER //
CREATE TRIGGER loginMonitoring BEFORE INSERT ON bought_tickets FOR EACH ROW
BEGIN
    INSERT INTO logs(date, operation_type, user) VALUES(CONVERT_TZ(NOW(), @@session.time_zone, '+00:00'), "bought_ticket", "client");
END //
DELIMITER ;

INSERT INTO users(username,password,role) VALUES("admin","admin","admin");
INSERT INTO users(username,password,role) VALUES("client","client","client");