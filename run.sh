#!/bin/bash
docker swarm leave --force
docker swarm init --advertise-addr 192.168.1.251
if [ $? -ne 0 ]
then
    echo "Docker swarm initialization failed"
    echo "Exiting..."
fi

docker service create --name registry --publish published=5000,target=5000 registry:2
if [ $? -ne 0 ]
then
    echo "Registry initialization failed"
    echo "Exiting..."
    docker swarm leave --force
fi

docker-compose -f stack.yml up -d --build
if [ $? -ne 0 ]
then
    echo "Docker compose build failed"
    echo "Exiting..."
    docker swarm leave --force
fi

docker-compose -f stack.yml down --volumes

docker-compose -f stack.yml push
if [ $? -ne 0 ]
then
    echo "docker push failed"
    echo "Exiting..."
    docker swarm leave --force
fi

echo "Preparing the env: Executing command docker stack deploy -c stack.yml idp"
docker stack deploy -c stack.yml idp

echo "Sleeeping a little(10 seconds)"
sleep 10

DATABASE=$(docker ps | grep database | cut -d " " -f 1)
CLIENT=$(docker ps | grep idp_client | cut -d " " -f 1)

echo "Set up the trigger for database"
docker exec -it $DATABASE \
    /bin/bash -c "mysql -ppassword AirplaneService < /var/lib/mysql1/trigger.sql" && sleep 1

echo "Last step: Starting the client"
docker exec -it $CLIENT \
    /bin/bash -c "java -jar /var/lib/client/client.jar"