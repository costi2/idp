package com.server.ServerSPRC.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsersRepository extends JpaRepository<Users, Integer> {
    Users findByUsername(String username);
}