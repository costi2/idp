#!/bin/bash

echo "Building client..."
cd client
rm ./exec/client.jar
mvn clean install -DskipTests
cp ~/.m2/repository/com/client/0.0.1-SNAPSHOT/client-0.0.1-SNAPSHOT.jar ./exec/client.jar
echo "Building client ended"

echo "Building server_client..."
cd .. 
cd server_client
rm ./exec/server.jar
mvn clean install -DskipTests
cp ~/.m2/repository/com/server/ServerSPRC/0.0.1-SNAPSHOT/ServerSPRC-0.0.1-SNAPSHOT.jar ./exec/server.jar
echo "Building server_client ended"

echo "Building server_admin..."
cd .. 
cd server_client
rm ./exec/server.jar
mvn clean install -DskipTests
cp ~/.m2/repository/com/server/ServerSPRC/0.0.1-SNAPSHOT/ServerSPRC-0.0.1-SNAPSHOT.jar ./exec/server.jar
echo "Building server_admin ended"
cd ..
