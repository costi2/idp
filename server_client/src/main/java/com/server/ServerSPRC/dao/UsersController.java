package com.server.ServerSPRC.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {
    @Autowired
    UsersRepository usersRepository;

    @RequestMapping(value = "/checkLogin", method = {RequestMethod.POST, RequestMethod.PUT})
    public String checkLogin(@RequestBody Users revUser){
        Users user = usersRepository.findByUsername(revUser.getUsername());
        if(user == null)
            return "failed";

        System.out.println(user.getRole());
        return user.getRole();
    }
}
